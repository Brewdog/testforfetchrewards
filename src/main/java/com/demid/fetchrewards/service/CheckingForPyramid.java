package com.demid.fetchrewards.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CheckingForPyramid {

    public Map<Character, Integer> countLetter(String word) {
        Map<Character, Integer> map = new HashMap<>();
        word.chars().forEach(i -> map.put((char) i, map.getOrDefault((char) i, 0) + 1));
//        for (int i = 0; i < word.length(); i++) { //it shows in a simplified way how this code works
//            Integer n = map.get(word.charAt(i));  //if streams are prohibited
//            if (n == null) {
//                map.put(word.charAt(i), 1);
//            }
//            else {
//                map.put(word.charAt(i), ++n);
//            }
//        }
        return map;
    }

    public boolean isPyramid(Map<Character, Integer> map) {
        if (map.isEmpty() || map.size() <= 1) {
            return false;
        }
        List<Integer> collect = map.values().stream().sorted().collect(Collectors.toList());
        int i = 0;
        while (i < collect.size() - 1) {
            if (collect.get(i) + 1 != collect.get(i + 1)) {
                return false;
            }
            i++;
        }
        return true;
    }
}
