package com.demid.fetchrewards.controller;

import com.demid.fetchrewards.json.ReqWord;
import com.demid.fetchrewards.service.CheckingForPyramid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import static org.springframework.http.ResponseEntity.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class PyramidCheckRest {
    private final CheckingForPyramid service;

    @PostMapping()
    public @ResponseBody
    ResponseEntity<String> checkWord(@RequestBody ReqWord req) {
        String checkingWord = req.getCheckingWord();
        if (checkingWord.trim().isEmpty() || checkingWord == null) {
            return badRequest().build();
        }
        return service.isPyramid(service.countLetter(checkingWord)) ? ok("It's a pyramid!") : ok("It isn't a pyramid");
    }
}
