package com.demid.fetchrewards.service;

import org.junit.Test;

import java.util.Map;

import static java.util.Optional.ofNullable;
import static org.junit.Assert.*;

public class CheckingForPyramidTest {
    CheckingForPyramid service = new CheckingForPyramid();

    @Test
    public void countLetter_regularUse() {
        Map<Character, Integer> banana = service.countLetter("banana");
        assertEquals(banana.size(), 3);
        assertEquals((long)banana.get('n'), 2);
        assertEquals((long)banana.get('a'), 3);
    }

    @Test
    public void isPyramid_regularUse() {
        boolean banana = service.isPyramid(service.countLetter("banana"));
        assertTrue(banana);
    }
    @Test
    public void isPyramid_wrongWord() {
        boolean banana = service.isPyramid(service.countLetter("bandana"));
        assertFalse(banana);
    }
}