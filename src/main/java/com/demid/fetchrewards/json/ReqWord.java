package com.demid.fetchrewards.json;

import lombok.Data;

@Data
public class ReqWord {
    private String checkingWord;
}
