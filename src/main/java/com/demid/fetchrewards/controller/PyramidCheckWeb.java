package com.demid.fetchrewards.controller;

import com.demid.fetchrewards.form.GetWordForm;
import com.demid.fetchrewards.service.CheckingForPyramid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;

@Controller
@RequiredArgsConstructor
@RequestMapping(value = {"/", "/web"})
public class PyramidCheckWeb {
    private final CheckingForPyramid service;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("form", new GetWordForm())
                .addAttribute("result", "word to check")
                .addAttribute("checkingWord", "Enter any ");
        return "index";
    }

    @PostMapping("/")
    public String checkWord(Model model,
                            @ModelAttribute("form") @Valid GetWordForm form) {

        String checkingWord = form.getCheckingWord();
        if (checkingWord.trim().isEmpty()) {
            model.addAttribute("form", form)
                    .addAttribute("result", "word field")
                    .addAttribute("checkingWord", "Empty ");
            return "index";
        }
        if (service.isPyramid(service.countLetter(form.getCheckingWord()))) {
            model.addAttribute("result", "is a pyramid!")
                    .addAttribute("checkingWord", form.getCheckingWord());
        } else {
            model.addAttribute("result", "isn't a pyramid")
                    .addAttribute("checkingWord", form.getCheckingWord());
        }
        return "index";
    }

}
