package com.demid.fetchrewards.form;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class GetWordForm {
    @NotNull
    @NotEmpty
    private String checkingWord;
    private String result;
}
